package com.wellsfromwales.sandbox;

public class Main {

	public static void main(String[] args) {
		SalesData salesData = new SalesData();
		displayGreeting();

		salesData.display();
//		new comment
//		another comment
	}
	
	private static void displayGreeting() {
		System.out.println("Hello happy sales people!");
		System.out.println("This app shows sales data");
		System.out.println("Test 1");
		System.out.println("More Test Code");
	}

}
